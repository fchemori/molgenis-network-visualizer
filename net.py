import streamlit as st
import molgenis.client
import networkx as nx
import pandas as pd
import plotly.graph_objects as go
import numpy as np
import matplotlib.pyplot as plt

# Connect to Molgenis API
session = molgenis.client.Session("https://data.disc4all.eu/api/")
username = st.text_input("Username:")
password = st.text_input("Password:", type='password')
entity = st.text_input("Entity:")
session.login(username, password)

# Get data from entity
df = pd.DataFrame(session.get(entity)
)

# Create an empty directed graph
G = nx.Graph()

# Get columns for nodes and edges
node1_col = st.selectbox("Select a column for the first node:", data.columns)
node2_col = st.selectbox("Select a column for the second node:", data.columns)
edge_col = st.selectbox("Select a column for the edges:", data.columns)

# Add edges to the graph based on the data
for index, row in data.iterrows():
    G.add_edge(row[node1_col], row[node2_col], weight=row[edge_col])

# Compute degree centrality
degree_centrality = nx.degree_centrality(G)
st.write("Degree centrality:", degree_centrality)

# Compute betweenness centrality
betweenness_centrality = nx.betweenness_centrality(G)
st.write("Betweenness centrality:", betweenness_centrality)

# Compute eigenvector centrality
eigenvector_centrality = nx.eigenvector_centrality(G)
st.write("Eigenvector centrality:", eigenvector_centrality)

# Create a report
st.write("Network analysis report:")
st.write("Number of nodes:", G.number_of_nodes())
st.write("Number of edges:", G.number_of_edges())
st.write("Density:", nx.density(G))
st.write("Clustering coefficient:", nx.average_clustering(G))

# Download report
if st.button("Download report"):
    with open("network_report.txt", "w") as f:
        f.write("Network analysis report:\n")
        f.write("Number of nodes: {}\n".format(G.number_of_nodes()))
        f.write("Number of edges: {}\n".format(G.number_of_edges()))
        f.write("Density: {}\n".format(nx.density(G)))
        f.write("Clustering coefficient: {}\n".format(nx.average_clustering(G)))



# @st.cache(show_spinner=False)
# def download_report(network_measures, network_data):
# pdf_file = f"network_report_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.pdf"
# pdfkit.from_string(generate_report_html(network_measures, network_data), pdf_file)
# st.success("Report generated and downloaded!")
# return pdf_file


